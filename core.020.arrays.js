core.arrays = function () {
    var _contains = function (array, criteria) {
        if (!core.is.array(array)) return false;

        for (var i = 0; i < array.length; i++) {
            if (core.is.object(criteria)) {
                if (core.objects.match(array[i], criteria)) {
                    return true;
                }
            }
            else if (core.is.function(criteria)) {
                if (criteria.call(array[i])) {
                    return true;
                }
            }
            else {
                if (array[i] == criteria) {
                    return true;
                }
            }
        }
        return false;
    };
    var _find = function (array, criteria) {
        var rtn = new Array();
        if (!core.is.array(array)) return rtn;

        for (var i = 0; i < array.length; i++) {
            if (core.is.object(criteria)) {
                if (core.objects.match(array[i], criteria)) {
                    rtn.push(array[i]);
                }
            }
            else if (core.is.function(criteria)) {
                if (criteria.call(array[i])) {
                    rtn.push(array[i]);
                }
            }
            else {
                if (array[i] == criteria) {
                    rtn.push(array[i])
                }
                ;
            }
        }
        return rtn;
    };
    var _remove = function (array, criteria) {
        var rtn = new Array();
        if (!core.is.array(array)) return rtn;

        for (var i = 0; i < array.length; i++) {
            if (core.is.object(criteria)) {
                if (!core.objects.match(array[i], criteria)) {
                    rtn.push(array[i]);
                }
            }
            else if (core.is.function(criteria)) {
                if (!criteria.call(array[i])) {
                    rtn.push(array[i]);
                }
            }
            else {
                if (array[i] != criteria) {
                    rtn.push(array[i]);
                }
            }
        }
        return rtn;
    };
    var _replace = function (array, criteria, val) {
        var rtn = new Array();
        if (!core.is.array(array)) return rtn;

        for (var i = 0; i < array.length; i++) {
            if (core.is.object(criteria) && core.objects.match(array[i], criteria)) {
                rtn.push(val);
            }
            else if (core.is.function(criteria) && criteria.call(array[i])) {
                rtn.push(val);
            }
            else if (array[i] == criteria) {
                rtn.push(val);
            }
            else {
                rtn.push(array[i]);
            }

        }
        return rtn;
    };
    var _clean = function (array) {
        var rtn = [];
        for (var i = 0; i < array.length; i++) {
            if (array[i] !== undefined && array[i] !== null && array[i] !== "" && array[i] !== NaN) {
                rtn.push(array[i]);
            }
        }
        return rtn;
    };

    return {
        contains: function (array, criteria) {
            return _contains(array, criteria);
        },
        find: function (array, criteria) {
            return _find(array, criteria);
        },
        findFirst: function (array, criteria) {
            var items = _find(array, criteria);
            if (items.length) {
                return items[0];
            }
        },
        remove: function (array, criteria) {
            return _remove(array, criteria);
        },
        replace: function(array, criteria, val) {
            return _replace(array, criteria, val);
        },
        clean: function(array) {
            return _clean(array);
        }
    }
}.call();