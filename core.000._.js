var core =
{
    publishes: {
        "onLoad": "window.onLoad",
        "append": "append"
    },
    alertListener: [],
    alert: function (msg) {
        if (core.alertListener) {
            if (core.is.function(core.alertListener)) {
                core.alertListener(msg);
                return;
            }
            if (core.is.array(core.alertListener)) {
                var bAlerted = false;
                for (var i = 0; i < core.alertListener.length; i++) {
                    if (core.is.function(core.alertListener[i])) {
                        core.alertListener[i](msg);
                        bAlerted = true;
                    }
                }
                if (bAlerted) {
                    return;
                }
            }
        }
        if (core.is.object(msg)) msg = core.objects.toString(msg);
        alert(msg);
    },
    empty: function (el) {
        if (!core.is.HTMLElement(el)) return;
        core.each(core.children(el), function () {
            core.remove(this);
        });
        while (el.firstChild) {
            el.removeChild(el.firstChild);
        }
    },
    each: function (obj, callback) {
        if (obj == undefined) { return; }
        if (core.is.string(obj)) { obj = core.get(obj); }
        var o = core.is.array(obj) ? obj : [obj];
        for (var i = 0; i < o.length; i++) {
            if (callback.call(o[i], i, o[i]) === false) {
                break;
            }
        }
    },
    extend: function (base, obj) {
        base = base || {};
        obj = obj || {};

        if (!core.is.object(base)) return (obj || base);
        if (!core.is.object(obj)) return (obj || base);

        var rtn = {};
        for (var i in base) {
            if (core.is.object(base[i])) {
                rtn[i] = core.extend(base[i], obj[i]); 
            }
            else rtn[i] = base[i];
        }
        for (var i in obj) {
            if (core.is.object(obj[i])) { if (!base[i]) rtn[i] = obj[i]; }
            else rtn[i] = obj[i];
        }
        return rtn;
    },

    get: function (selector, element) {
        return Sizzle(selector, element);
    },
    children: function (el) {
        if (!core.is.HTMLElement(el)) return;
        var r = [];
        for (var i = el.childNodes.length >>> 0; i--;) {
            r[i] = el.childNodes[i];
        }
        return r;
    },
    getSingle: function (selector, element) {
        var r = core.get(selector, element);
        if (r.length > 0) return r[0];
    },
    getParentbyClass: function(element, className){
        var rtn = element;
        if (!core.is.HTMLElement(rtn)) return;

        if (className) {
            while (rtn && !core.className.check(rtn, className)) {
                if (rtn === document.body) break;
                rtn = rtn.parentNode;
            }
        }
        return rtn;
    },
    getParentbyAttribute: function(element, name, value) {
        var rtn = element;
        if (!core.is.HTMLElement(rtn)) return;

        while (rtn) {
            var aValue = rtn.getAttribute(name);
            if (aValue !== undefined && aValue !== null) {
                if (value) {
                    if (core.arrays.contains(aValue.split(','), value)) break;
                }
                else break;
            }
            if (rtn.parentNode === document) break;
            rtn = rtn.parentNode;
        }
        return rtn;
    },
    append: function (parent, element, index) {
        if (!core.is.HTMLElement(parent)) return;

        var o = [];
        var s = [];

        var enforceClearFloat = function (parent, child) {
            var clear = undefined;
            core.each(parent.childNodes, function () {
                if (this === parent) return true;
                if (core.className.check(this, "clearFloats")) {
                    clear = this;
                    return false;
                }
            });
            if (clear) {
                parent.removeChild(clear);
                parent.appendChild(child);
                parent.appendChild(clear);
            }
            else {
                parent.appendChild(child);
            }
        };

        if (core.is.HTMLElement(element)) {
            o.push(element);
        }
        else {
            var e = document.createElement("div");
            e.innerHTML = core.strings.trim.all(element);

            core.each(core.get("script", e), function () {
                s.push(this.innerHTML);
                core.remove(this);
            });

            for (var i = 0; i < e.childNodes.length; i++) {
                if (e.childNodes[i] === e) continue;
                o.push(e.childNodes[i]);
            }
        }

        if (core.is.HTMLElement(index)) {
            if (index.parentNode != parent) {
                for (var i = 0; i < o.length; i++) {
                    enforceClearFloat(parent, o[i]);
                }
            }
            else {
                for (var i = 0; i < o.length; i++) {
                    parent.insertBefore(o[i], index);
                }
            }
        }
        else if (core.is.number(index)) {
            for (var i = 0; i < o.length; i++) {
                if (!parent.childNodes[index]) {
                    enforceClearFloat(parent, o[i]);
                }
                else {
                    parent.insertBefore(o[i], parent.childNodes[index++]);
                }
            }
        }
        else {
            for (var i = 0; i < o.length; i++) {
                enforceClearFloat(parent, o[i]);
            }
        }

        if (s.length > 0) eval(s.join(" "));

        if (core.pubsub) {
            core.pubsub.publish(core.publishes.append, {container: parent});
        }
    },
    remove: function(element) {
        if (element && element.parentNode)
            element.parentNode.removeChild(element);
    },
    value: function (el, value) {
        if (!core.is.HTMLElement(el)) return undefined;
        
        var R = "";
        var F = el.getAttribute('gp-format');
        var V = (F ? (core.format.to(value, F) || '') : (value || '').toString());

        switch (el.nodeType) {

            case 1: // ELEMENT_NODE
                
                var N = el.nodeName.toLowerCase();
                if (N == "input") {
                    var T = (el.getAttribute('type') ? el.getAttribute('type').toLowerCase() : "");
                    switch (T) {
                        case "checkbox":
                        case "radio":
                            if (arguments.length > 1)
                                el.checked = core.cast.boolean(value);
                            R = el.checked;
                            break;
                        default:
                            if (arguments.length > 1)
                                el.value = V;
                            R = el.value;
                            break;
                    }
                }
                else if (N == "select") {
                    if (arguments.length > 1) {
                        el.selectedIndex = undefined;
                        core.each(core.get('option', el), function () {
                            if ((this.value || this.innerHTML) == value.toString()) {
                                this.selected = true;
                                return false;
                            }
                        });
                    }

                    var sOpt = core.getSingle('option:selected', el);
                    if (sOpt) R = (sOpt.value || sOpt.innerHTML);
                }
                else if (N == "img") {
                    if (arguments.length > 1)
                        el.setAttribute('src', value.toString());
                    R = (el.getAttribute('src') || '');
                }
                else if (N == "a") {
                    if (arguments.length > 1)
                        el.setAttribute('href', value.toString());
                    R = el.getAttribute('href');
                }
                else if (N == "textarea") {
                    if (arguments.length > 1)
                        el.value = V;
                    R = el.value;
                }
                else {
                    if (arguments.length > 1)
                        el.innerHTML = V;
                    R = el.innerHTML;
                }

                break;

            case 3: // TEXT_NODE
                if (arguments.length > 1)
                    el.nodeValue = V;
                R = el.nodeValue;
        }

        return R.toString();
    },
    focus: function(el) {
        setTimeout(function () {
            if (core.is.HTMLElement(el)) return;

            if (!el.getAttribute("tab-index")) {
                el.setAttribute("tabindex", "-1");
            }
            el.focus();
        });
    },
    guid: function (val) {
        if (core.cast.number(val) === 0) {
            return '00000000-0000-0000-0000-000000000000';
        }

        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });
        return uuid;
    }
};
