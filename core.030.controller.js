//TODO: Research IE11 issue with javascript closures; it doesn't handle controllers
//being created enclosed within a local scope. refactor to be closure friendly in IE.

//TODO: refactor "_create" method - pay attention to create/destroy methodology
//should expose "destroy" property on the creation options. right now, the destroy/build
//methods assume an array which doesn't actually make sense to expose (i.e. to force
//the user to have to know about the array of functions the destroy method is transformed
//into). the idea was originally to chain destruction request 
//on a controller at any point after its inception to give the caller insight into 

//the controller state. this can still happen, it just needs to be refactored to expose
//only the non-underscore function within the create/destroy/build methods. Also
//I should be able to call the "controller.destroy()" method to start the chain reaction
//that eventually removes the controller and its elements completely from the screen
//note: I may still have to handle possible memory leaks- IE DOM to function leakage
//when DOM elements have a reference to a JS object/function and the JS object/function
//has a reference to the DOM element and is deleted will cause leakage
//not sure if this is still an issue in the latest version of IE (still not to be trusted b/c IE doesn't
//support JS closures well.. ref: http://javascript.crockford.com/memory/leak.html

//TODO: the transferance of data between parent/child controller is a bit clanky. need
//a way to transfer state/DOM to the child controller in a way that does not involve
//complex destruction logic in the child controller. one way to do this is to inject
//state in the data object or elements in the elements object from parent to child. 
core.controller =
{
    types: new Object(),
    surfaces: {
        "body": document.body,
        "default": document.body
    },
    _config: {
        create: [],
        build: [],
        destroy: [],
        elements: {},
        data: {},
        onCreate: undefined,
        onBuild: undefined,
        onDestroy: undefined,
        surface: undefined,
        focus: true
    },

    _get: function (aName) {
        var N = (aName || "").toLowerCase();
        var rtn = undefined;
        for (var x in core.controller.types) {
            if (x.toLowerCase() == N) {
                rtn = core.objects.clone(core.controller.types[x]);
                rtn.name = N;
                break;
            }
        }
        return rtn;
    },

    _create: function (obj) {
        if (!obj) return;
        if (!core.is.object(obj)) obj = { name: obj };
        var o = core.controller._get(obj.name);
        if (!o) throw core.strings.format("[core.controller.create] could not find a known type for the given name: {name}", { find: "{name}", replace: (obj.name || obj) });

        var _surface = o.surface || obj.container;

        var _create = [];
        var _build = [];
        var _destroy = [];

        if (core.is.array(o.create))
            _create = _create.concat(o.create);
        if (core.is.array(obj.create))
            _create = _create.concat(obj.create);

        if (core.is.array(o.build))
            _build = _build.concat(o.build);
        if (core.is.array(obj.build))
            _build = _build.concat(obj.build);

        if (core.is.array(o.destroy))
            _destroy = _destroy.concat(o.destroy);
        if (core.is.array(obj.destroy))
            _destroy = _destroy.concat(obj.destroy);

        delete obj.create;
        delete obj.build;
        delete obj.destroy;

        delete o.create;
        delete o.build;
        delete o.destroy;

        //Bug fix: controller was being overwritten by controller template. this is bad
        //controller should always be overwritten by data element of incoming options
        o = core.extend(core.extend(core.objects.clone(core.controller._config), o), obj)

        if (_surface) {
            if (core.is.string(_surface)) {
                o.surface = core.controller.surfaces[_surface];
            }
            if (core.is.HTMLElement(_surface)) {
                o.surface = _surface;
            }
        }
        o.id = core.guid();

        o.destroy.push(function () {
            var handler = core.controller.get(o.id);
            if (core.is.function(o._destroy))
                if (o._destroy.call(o) === false) return false;

            var i = [];
            core.each(core.get('[controller-id]', handler.surface), function () { i.push(this.getAttribute("controller-id")); });
            core.each(i, function () {
                if (!core.controller.get(this))
                    return true;
                core.controller.get(this).onDestroy();
            });

            core.empty(handler.surface);
        });


        //note: this should actully be a little more apparent in the ".build()" function
        //the setting of the default container.. shouldn't be stuffed in the ".onCreate method
        //BUG: the create method below should be the first element on the array to get executed then
        //subsequent methods, not the other way around which is how it currently works. 
        //should either iterate the array backwards for execution or persist the function below
        //first before any other create methods are called. 
        o.create.push(function () {
            if (!this.surface.core)
                this.surface.core = new Object();
            this.surface.core.controller = this;
            this.surface.setAttribute("controller-id", this.id);
            this.surface.setAttribute("controller", this.name);

            if (core.is.string(this.url))
                core.browser.location.push(undefined, this.url);
        });

        o.onCreate = function () {
            for (var i = this.create.length - 1; i >= 0; i--)
                if (this.create[i].call(this) === false)
                    return false;
        };

        o.onBuild = function () {
            for (var i = this.build.length - 1; i >= 0; i--)
                if (this.build[i].call(this) === false)
                    return false;
        };

        o.onDestroy = function () {
            for (var i = this.destroy.length - 1; i >= 0; i--)
                if (this.destroy[i].call(this) === false)
                    return false;

            if (core.is.function(this.onDestroyed))
                this.onDestroyed.call(this);
        };

        for (var x in _create)
            o.create.push(_create[x]);

        for (var x in _build)
            o.build.push(_build[x]);

        for (var x in _destroy)
            o.destroy.push(_destroy[x]);

        return o;
    },

    get: function (val, name) {
        if (!core.is.string(name)) {
            if (core.is.HTMLElement(val))
                val = val.getAttribute("controller-id");
            var el = core.getSingle(core.strings.format('[controller-id="{id}"]', { find: "{id}", replace: val }));
            if (el && el.core)
                return el.core.controller;
        } else {
            if (!core.is.HTMLElement(val))
                return undefined;
            var el = core.elements.find({ element: val, attribute: "controller-id", attributeValue: name });
            if (el && el.core)
                return el.core.controller;
        }
    },

    url: function () {
        var a = window.location.pathname.split("?");
        if (a.length == 0 || !a[0]) return;

        var c = undefined;
        for (var x in core.controller.types) {
            if (core.controller.types[x].url == a[0]) {
                c = x;
                break;
            }
        }

        if (!c) return;

        var d = undefined;
        if (a.length > 1) d = a[1];

        var rtn = {
            "name": c,
            "data": d
        };

        return rtn;
    },

    build: function (options) {
        if (options == undefined) {
            console.error("no controller information passed to controller.build");
            return;
        }

        if (core.is.string(arguments[0])) {
            options = {
                name: arguments[0]
            }
        }

        if (core.is.object(options)) {
            if (!options.container && core.is.HTMLElement(arguments[1]))
                options.container = arguments[1];
            if (!options.container)
                options.container = core.controller.surfaces["default"];
        }

        var A = [];

        if (core.is.HTMLElement(options)) {
            var data = [];
            core.each(core.get("[controller]", options), function () {
                data.push({
                    "name": this.getAttribute("controller"),
                    "container": this,
                    "data": JSON.parse(this.getAttribute("controller-data")),
                    "type": undefined
                });
            });
            options = data;
        }

        core.each(options, function () {
            var action = core.controller._create(this);
            if (core.controller.get(action.surface))
                core.controller.get(action.surface).onDestroy();

            core.empty(action.surface);
            action.onCreate();
            if (action.view) {
                core.empty(action.surface);
                core.views.get(action.view, action.surface);
            }
            if (!action.elements) action.elements = [];
            core.each(core.get("[id]", action.surface), function () {
                action.elements[this.getAttribute("id")] = this;
            });

            action.init();
            action.onBuild();

            if (action.focus) core.focus(action.surface);

            delete action.container;

            A.push(action);

            if (console && console.log)
                console.log(core.strings.format("Build {action}", [{ find: "{action}", replace: action.name || 'unknown' }]));
        });

        return A;
    },

    find: function (element) {

        if (core.is.object(element))
            return element;

        var rtn = core.getParentbyAttribute(element, "controller-id");

        if (!rtn["core"])
            return undefined;

        return rtn["core"]["controller"];
    },

    exist: function (name) {
        return !!core.controller.types[name];
    }
};