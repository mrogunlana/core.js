core.views = function() {
    var _version = "0.0.0";
    var _views = new Object();
    var _add = function (url, val) {
        _views[url] = core.strings.trim.all(val);
    };
    var _get = function (url, container, data) {
        if (core.is.object(url)) {
            url = (url.url | url);
            container = (url.container || container);
            data = (url.data || data)
        }

        if (!url){ throw "view url not provided";}
        var rtn = '';

        if (core.is.string(_views[url])) {
            rtn = core.objects.clone(_views[url]);
        }
        else {
            var _url = url;
            if (_version) {
                if (/\?/g.test(url)) {
                    url += "&v=" + _version;
                }
                else {
                    url += "?v=" + _version;
                }
            }

            core.ajax.send({
                type: 'GET',
                dataType: 'text',
                async: false,
                cache: true,
                url: url,
                success: function (data) {
                    rtn = core.objects.clone(data);
                    core.views.add(_url, rtn);
                },
                error: function (data, xhr) {
                    rtn = '[' + xhr.status + '] ' + xhr.statusText + ' [' + (url || 'undefined url') + ']';
                    return true;
                }
            });
        }

        if (container) {
            core.append(container, rtn);
            container.setAttribute("bpcp-view", url);
            if (core.is.object(data)) {
                core.data.set(container, data);
            }
        }

        return rtn;
    }
    return {
        version: function () {
            if (!!arguments[0]) {
                _version = arguments[0];
            }
            return _version;
        },
        add: function(url, val){
            return _add(url,val);
        },
        get: function(url, container, data) {
            return _get(url, container, data);
        },
        clear: function() {
            _views = new Object();
        }
    }
}.call();
