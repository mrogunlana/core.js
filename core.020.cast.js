core.cast = function () {
    var _boolean = function(val) {
        switch (val) {
            case true:
            case 'true':
            case 'TRUE':
            case 'True':
            case 1:
            case '1':
            case 'on':
            case 'ON':
            case 'On':
            case 'yes':
            case 'YES':
            case 'Yes':
                return true;
                break;
            default:
                return false;
                break;
        }
    };
    var _date = function (val) {
        function _String(value) {
            var RE = /^\/Date\(/;
            if (RE.test(value)) {
                try {
                    var v = value.substring(1, value.length - 1);
                    if (v.split("-").length > 2) {
                        v = core.strings.format(v, "-", "/");
                    }
                    var tDate = eval("new " + v);
                    if (core.is.date(tDate)) {
                        value = tDate;
                    }
                }
                catch (err) {
                }
            }
            else {
                var regexIso8601 = /^(\d{4}|\+\d{6})(?:-(\d{2})(?:-(\d{2})(?:T(\d{2}):(\d{2}):(\d{2})\.(\d{1,3})(?:Z|([\-+])(\d{2}):(\d{2}))?)?)?)?$/;

                if (regexIso8601.test(value)) {
                    var m = regexIso8601.exec(value);
                    if (m && m.length > 3 && m[1].length < 5 && m[2] < 13 && m[3] < 32) {
                        if (Date.parse('2011-11-29T15:52:30.5') !== 1322581950500 || Date.parse('2011-11-29T15:52:30.52') !== 1322581950520 || Date.parse('2011-11-29T15:52:18.867') !== 1322581938867 || Date.parse('2011-11-29T15:52:18.867Z') !== 1322581938867 || Date.parse('2011-11-29T15:52:18.867-03:30') !== 1322594538867 || Date.parse('2011-11-29') !== 1322524800000 || Date.parse('2011-11') !== 1320105600000 || Date.parse('2011') !== 1293840000000) {
                            try {
                                var dt = new Date(Date.UTC(m[1], (m[2] || 1) - 1, m[3] || 1, m[4] - (m[8] ? m[8] + m[9] : 0) || 0, m[5] - (m[8] ? m[8] + m[10] : 0) || 0, m[6] || 0, ((m[7] || 0) + '00').substr(0, 3)));
                                if (core.isDate(dt)) {
                                    value = dt;
                                }
                            } catch (e) {
                            }
                        }
                        else {
                            try {
                                var dt = new Date(value);
                                if (core.is.date(dt)) {
                                    value = dt;
                                }
                            } catch (e) {
                            }
                        }
                    }
                }
            }
            return value;
        }

        if (val == undefined) return undefined;
        if (core.is.date(val)) return val;

        if (core.is.string(val)) {
            val = _String(val);
            if (core.is.date(val)) return val;
            else if (core.is.numeric(val)) return undefined;
        }

        return val;
    };
    var _number = function(val, percision) {
        var rtn = Number(val);
        if (isNaN(rtn)) {
            return undefined;
        }

        if (core.is.number(percision)) {
            return rtn.toFixed(percision);
        }
        return rtn;
    };
    var _string = function(val) {
        return String(val);
    };
    return {
        boolean: function(val) {
            return _boolean.apply(this,arguments);
        },
        date: function(val) {
            return _date.apply(this,arguments);
        },
        number: function(val, percision) {
            return _number.apply(this,arguments);
        },
        string: function(val) {
            return _string.apply(this,arguments);
        }
    }
}.call();