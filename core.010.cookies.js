core.cookies = function() {
    var _available = undefined;
    var _check = function () {
        if (_available === undefined) {
            _test();
        }
        if (!_available) {
            var msg = {title: "Browser Settings Error", body: "Cookies must be enabled to use this web site."};
            core.log(msg);
            core.alert(msg);
        }
        return _available;
    };
    var _get = function (name) {
        var exp = new RegExp(encodeURI(name) + "=([^;]+)");
        if (exp.test(document.cookie + ";")) {
            exp.exec(document.cookie + ";");
            try {
                return JSON.parse(decodeURI(RegExp.$1));
            } catch (ex) {
                return decodeURI(RegExp.$1);
            }
        }
    };
    var _set = function (name, value, date) {
        var val = JSON.stringify(value);
        document.cookie = encodeURI(name) + "=" + encodeURI(val) + (core.is.date(date) ? "; expires=" + date.toGMTString() : "" ) + "; path=/";
    };
    var _remove = function (name) {
        document.cookie = encodeURI(name) + "=" + encodeURI('Deleted Cookie') + "; expires=" + (new Date(2011, 1, 1).toGMTString()) + "; path=/";
    };
    var _test = function () {
        core.cookies.available = true;
        document.cookie = "testSessionCookie=Enabled";
        if (core.cookies.get("testSessionCookie") != "Enabled") {
            _available = false;
            return false;
        }
        else {
            _remove("testSessionCookie");
            _available = true;
            return true;
        }
    };
    return {
        available: function () {
            return _check();
        },
        get: function (name) {
            if (_check) {
                return _get(name);
            }
        },
        set: function (name, value, date) {
            if (_check) {
                return _set(name, value, date);
            }
        },
        remove: function (name) {
            if (_check) {
                return _remove(name);
            }
        }
    }
}.call();