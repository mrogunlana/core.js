﻿core.validate = function () {

    //note: example call to this library
    //core.validate.data(core.data.get(), [{
    //    name: "first",
    //    validator: function (data) { }, OR "required"
    //    complete: function () { },
    //}]);

    //assumptions:
    //1. first argument will be an object
    //2. second argument (validators) will be an array of validator objects
    //3. the "validator" property within the array of validators
    //   will either be a function or a string
    //4. the "add" function will be a named validator function
    //5. returns an object with a valid property
    //6. "complete" function will run at the end of rule validation and pass result

    //use cases
    //1. code for instance that "validator" is a string, check in list of validators
    //   and execute that function instead
    //2. code for multiple validators per name

    var _rules = {};
    var _validators = function (validator) {
        var me = this;
        if (core.is.function(validator))
            return core.extend(me, { validator: validator });
        if (!core.is.string(validator))
            return me;
        if (validator.indexOf(",") < 0)
            return core.extend(me, core.objects.clone(_rules[validator]));
        
        var R = core.extend(me, { validator: [] }); 
        core.each(validator.split(","), function () {
            R.validator.push(_rules[core.strings.trim.all(this)]);
        });
        return R;
    };
    var _data = function (data, validators) {
        var fields = Object.keys(data) || {};
        var results = [];

        for (var i in fields) {
            core.each(core.arrays.find(validators, { name: fields[i] }), function () {
                var rule = this;

                rule = _validators.call(rule, rule.validator);
                rule.data = data[fields[i]];

                if (core.is.function(rule.validator))
                    rule.valid = rule.validator.call(rule);
                if (core.is.array(rule.validator))
                    rule = (function (R) {
                        var V = [];
                        core.each(R.validator, function () {
                            var O = this.validator.call(R);
                            if (!O)
                                R = core.extend(R, this);
                            V.push(O);
                        });
                        R.valid = !core.arrays.contains(V, false);
                        return R;
                    })(rule);
                if (core.is.function(rule.complete))
                    rule.complete.call(rule);

                results.push(rule);
            });
        }
        
        return core.arrays.find(results, { valid: false }).length == 0;
    };
    return {
        "add": function (name, rule) {
            return _rules[name] = rule;
        },
        "data": function (data, validators) {
            return _data(data, validators);
        }
    }
}.call();

core.validate.add("required", {
    message: "The {field} field is required.",
    validator: function () {
        return !(!this.data || this.data === '' || this.data === undefined);
    }
});
core.validate.add("email", {
    message: "The {field} field must contain a valid email address.",
    validator: function () {
        var R = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        return R.test(this.data);
    }
});
core.validate.add("url", {
    message: "The {field} field must contain a valid URL.",
    validator: function () {
        var R = /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/;
        return R.test(this.data);
    }
});
core.validate.add("min-length", {
    message: "The {field} field must be at least %s characters in length.",
    validator: function () {
        var R = /^[0-9]+$/;
        if (!R.test(this.data)) return false;
        if (!core.is.function(this.length)) return false;

        var L = this.length.call(this);

        return (this.data >= parseInt(L, 10));
    }
});
core.validate.add("phone", {
    message: "The {field} field must contain a valid phone number.",
    validator: function () {
        if (!this.data)
            return true;
        if (!this.data.match(/\d/g))
            return false;
        return this.data.match(/\d/g).length >= 10
    }
});