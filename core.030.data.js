core.data = function() {
    var _checkElement = function(element) {
        if (!core.is.HTMLElement(element)) throw "The element must be an HTML element. [core.data.set]";
    }
    var _clear = function (element) {
        var data= new Object();
        core.each(core.get('[gp-name]', element), function () {
            data[this.getAttribute("gp-name")] = undefined;
        });
        core.data.set(element, data);
    };
    var _get = function (element) {
        var rtn = new Object();
        core.each(core.get('[gp-name]', element), function () {
            var T = this.getAttribute('type');
            var F = this.getAttribute('gp-format');
            var N = this.getAttribute('gp-name');
            var L = this.getAttribute('gp-data-list');

            if (T == 'checkbox') {
                rtn[N] = !!this.checked;
                return;
            } else if (L) {
                rtn[N] = _getList(this);

            } else if (["div", "span"].indexOf(this.nodeName.toLowerCase()) != -1) {
                rtn[N] = this.innerText || this.textContent;
            }
            else {
                rtn[N] = core.value(this);
            }

            if (F) {
                rtn[N] = core.format.from(rtn[N], F);
            }
        });

        return rtn;
    };
    var _set = function (element, data, onRender) {
        data = core.objects.flatten(data);
        for (var n in data) {
            core.each(core.get('[gp-name="' + n + '"]', element), function () {
                if (core.is.array(data[n])) {
                    _setList(this, data[n], this.getAttribute("gp-view"));
                } else if (core.is.object(data[n])) {

                }
                else
                    core.value(this, data[n]);
            });
        }
        if (core.is.function(onRender)) {
            onRender(element, data);
        }
    };
    var _getList = function (element) {
        var rtn = new Array();
        core.each(core.children(element), function () {
            rtn.push(core.data.get({ element: this }));
        });
        return rtn;
    };
    var _setList = function (element, data, view, onPreRender, onRender) {
        element.setAttribute("gp-data-list","1");

        for (var i = 0; i < data.length; i++) {
            if (data[i]) {
                core.append(element, core.views.get(view));
                var children = core.children(element);
                if (children && children.length) {
                    core.each(children[children.length - 1],
                        function () {
                            if (core.is.function(onPreRender)) {
                                onPreRender(this, data[i], i);
                            }
                            _set(this, data[i], onRender);
                        });
                }
            }
        }
    };
    return {
        clear: function (element) {
            if (core.is.object(element)) {
                element = (element.element || element);
            }
            _checkElement(element);
            return _clear(element);
        },
        get: function (element) {
            if (core.is.object(element)) {
                element = (element.element || element);
            }
            _checkElement(element);
            return _get(element);
        },
        set: function (element, data, onRender)  {
            if (core.is.object(element)) {
                data = (data || element.data);
                onRender = (onRender || element.onRender);
                element = (element.element || element);
            }
            _checkElement(element);
            return _set(element, data, onRender) ;
        },
        list: {
            get: function (element) {
                if (core.is.object(element)) {
                    element = (element.element || element);
                }
                _checkElement(element);
                return _getList(element);
            },
            set: function (element, data, view, onPreRender, onRender) {
                if (core.is.object(element)) {
                    data = (data || element.data);
                    view = (view || element.view);
                    onPreRender = (onPreRender || element.onPreRender);
                    onRender = (onRender || element.onRender);
                    element = (element.element || element);
                }
                _checkElement(element);
                return _setList(element, data, view, onPreRender, onRender);
            }
        }
    }
}.call();
