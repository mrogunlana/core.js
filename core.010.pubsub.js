core.pubsub = function(){
    var _events = {};
    var _subscribe = function (pubName, fn, desc) {
        var id = core.guid();

        if (_events[pubName] == undefined) {
            _events[pubName] = new Array();
            _events[pubName].push({ fn: fn, desc: desc  });
        }
        else {
            var Events = _events[pubName];
            var functionExists = false;

            for (var i = 0; i < Events.length; i++)
                if (fn == Events[i]["fn"]) {
                    functionExists = true;
                    break;
                }

            if (!functionExists)
                _events[pubName].push({ fn: fn, desc: desc, id: id });
        }
        return id;
    };
    var _publish = function (pubName, data) {
        if (_events[pubName]) {
            var events = _events[pubName];

            for (var i = 0; i < events.length; i++) {
                try {
                    if (events[i]["fn"](data) === false) {
                        _unSubscribe(pubName, events[i]["fn"]);
                    }
                } catch (e) {
                    core.log.handler(e, core.strings.format('[core.pubsub.publish][{name}][{desc}]', [
                        { find: '{name}', replace: pubName },
                        { find: '{desc}', replace: (events[i] || {})["desc"] }
                    ]));
                }
            }
        }
    };
    var _unSubscribe = function (pubName, id) {
        var Events = _events[pubName];

        if (Events) {
            for (var i = 0; i < Events.length; i++) {
                if (Events[i]["id"] === id) {
                    Events = Events.splice(i, 1);
                    if (_events[pubName].length == 0) {
                        delete _events[pubName];
                    }
                    break;
                }
            }
        }
    };
    return {
        subscribe: function (pubName, fn, desc) {
            return _subscribe.apply(this, arguments);
        },
        publish: function (pubName, data) {
            return _publish.apply(this, arguments);
        },
        unsubscribe: function(pubName, id) {
            return _unSubscribe.apply(this, arguments);
        }
    }
}.call();
