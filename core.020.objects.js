core.objects = function() {
    var _clone= function (obj) {
        if (obj == undefined) return undefined;

        var rtn = new Object();

        if (core.is.primitive(obj)) {
            rtn = obj;
            return rtn;
        }
        if (core.is.HTMLElement(obj) || core.is.function(obj)) { return obj; }

        if (core.is.array(obj)) {
            rtn = new Array();
            for (var i = 0; i < obj.length; i++) {
                rtn.push(_clone(obj[i]));
            }
            return rtn;
        }

        for (var i in obj) {
            if (obj[i] == undefined) {
                rtn[i] = undefined;
            }
            else if (core.is.object(obj[i]) || core.is.array(obj[i])) {
                rtn[i] = _clone(obj[i]);
            }
            else {
                rtn[i] = obj[i];
            }
        }

        return rtn;
    };
    var _expand= function (obj) {
        var rtn = new Object();
        for (var i in obj) {
            var aKeys = i.split('.');
            var currLevel = rtn;
            if (aKeys.length == 1) {
                rtn[i] = obj[i];
            }
            else {
                for (var n = 0; n < aKeys.length - 1; n++) {
                    if (!core.is.object(currLevel[aKeys[n]])) {
                        currLevel[aKeys[n]] = new Object();
                    }
                    currLevel = currLevel[aKeys[n]];
                }
                currLevel[aKeys[aKeys.length - 1]] = obj[i];
            }
        }

        return rtn;
    };
    var _flatten = function (obj) {
        var rtn = new Object();

        function _build(key, val) {
            if (core.is.HTMLElement(val)) {
                /* ignore HTML Elements */
            }
            else if (core.is.object(val)) {
                for (var n in val) {
                    _build(key + '.' + n, val[n]);
                }
            }
            else {
                rtn[key] = val;
            }
        }

        for (var i in obj) {
            _build(i, obj[i]);
        }

        return rtn;
    };
    var _match = function (Obj, Criteria) {

        if (!core.is.object(Criteria)) return true;
        if (!core.is.object(Obj)) return false;

        var Ret = true;
        for (var k in Criteria) {
            if (core.is.function(Obj[k])) {
                var cFn = '';
                if (core.is.function(Criteria[k])) {
                    cFn = Criteria[k].toString();
                }
                if (cFn != Obj[k].toString()) {
                    Ret = false;
                    break;
                }
            }
            else if (core.is.object(Obj[k])) {
                if (!_match(Obj[k], Criteria[k])) {
                    Ret = false;
                    break;
                }
            }
            else if (Criteria[k] != Obj[k]) {
                Ret = false;
                break;
            }
        }

        return Ret;
    };

    return {
        "clone": function(obj) {
            return _clone(obj);
        },
        "expand": function(obj) {
            return _expand(obj);
        },
        "flatten": function(obj) {
            return _flatten(obj);
        },
        "match": function(obj, criteria) {
            return _match(obj, criteria);
        }
    }
}.call();
