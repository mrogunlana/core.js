core.ajax = function () {
    var __config = {
        type: 'POST',
        dataType: 'text',
        data: undefined,
        async: true,
        polling: false,
        cache: false,
        headers: {}
    };
    var _config = function() {
        if (core.is.object(arguments[0])) {
            __config = core.extend(__config, arguments[0]);
        }
        return __config;
    };
    var _send = function() {
        var options = core.objects.clone(__config);
        if (core.is.string(arguments[0])) {
            options.url = arguments[0];
        }
        if (core.is.object(arguments[0])) {
            options = core.extend(options, arguments[0]);
        }
        if (core.is.object(arguments[1])) {
            options.data = arguments[1];
        }

        var params = function (d) {
            var r = [];
            for (x in d) {
                if (!d[x]) continue;
                if (core.is.array(d[x]) || core.is.object(d[x]))
                    r.push(x + "=" + encodeURIComponent(core.objects.stringify(d[x])));
                else
                    r.push(x + "=" + encodeURIComponent(d[x]));
            }
            return r.join("&");
        }

        var xhr = (window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP"));
        var rtn = undefined;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                rtn = xhr.responseText;
                if (core.is.JSON(rtn)) {
                    rtn = JSON.parse(rtn);
                }
                if (xhr.status > 0 && xhr.status < 400) {
                    if (core.is.function(options.success)) {
                        options.success(rtn, xhr);
                    }
                }
                else {
                    if (core.is.object(rtn))
                        if (core.is.string(rtn.ExceptionMessage))
                            if (!!console && !!console.log)
                                console.log(core.strings.format("ERROR: {MESSAGE}\r\nTRACE: {TRACE}", [
                                    { find: "{MESSAGE}", replace: rtn.ExceptionMessage },
                                    { find: "{TRACE}", replace: rtn.StackTrace }
                                ]));
                    if (core.is.function(options.error)) {
                        options.error(rtn, xhr);
                    }
                }
                if (core.is.function(options.complete)) {
                    options.complete(rtn, xhr);
                }
            }
        };

        if (options.type.toLowerCase() == "get") {
            options.url = options.url + core.queryString.create(options.data);

            xhr.open(options.type, options.url, options.async);
            for (var x in options.headers) {
                if (options.headers[x]) {
                    xhr.setRequestHeader(x, options.headers[x])
                }
            }
            xhr.send();
        }
        if (options.type.toLowerCase() == "post" ||
            options.type.toLowerCase() == "put") {
            xhr.open(options.type, options.url, options.async);

            if (!options.headers["Content-type"])
                options.headers = core.extend(options.headers, {"Content-type": "application/json"});
            for (var x in options.headers) {
                if (options.headers[x]) {
                    xhr.setRequestHeader(x, options.headers[x])
                }
            }
            if (options.headers["Content-type"] && options.headers["Content-type"] == "application/json")
                xhr.send(core.is.string(options.data) ? options.data : JSON.stringify(options.data));
            else
                xhr.send(core.is.string(options.data) ? options.data : params(options.data));
        }
        return rtn;
    };
    return {
        "send": function() {
            return _send.apply(this,arguments);
        },
        config: function() {
            return _config.apply(this, arguments);
        }
    }
}.call();