core.is = function () {
    return {
        array: function (obj) {
            return !!(obj instanceof Array);
        },
        boolean: function (obj) {
            return (typeof obj) === "boolean";
        },
        date: function (obj) {
            return !!(obj instanceof Date);
        },
        function: function (obj) {
            return !!(obj instanceof Function);
        },
        HTMLElement: function (obj) {
            return !!(obj instanceof HTMLElement);
        },
        HTMLText: function (obj) {
            if (!core.is.HTMLElement(obj)) return false;
            return obj.nodeType === 3;
        },
        JSON: function (obj) {
            if (core.is.string(obj)) {
                return (/^(\{|\[).*(\}|\])$/.test(obj));
            }
            return false;
        },
        number: function (obj) {
            return (typeof obj) === "number"
        },
        numeric: function (obj) {
            if (core.is.number(obj)) return true;
            if (core.is.string(obj)) {
                var RE = /^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/g;
                return (RE.test(obj));
            }
            return false;
        },
        object: function (obj) {
            if (obj == undefined || obj == null) return false;

            if (core.is.primitive(obj)) return false;
            if (core.is.array(obj)) return false;
            if (core.is.function(obj)) return false;
            if (core.is.HTMLElement(obj)) return false;

            return !!(obj instanceof Object);
        },
        string: function (obj) {
            return (typeof obj) === "string";
        },
        primitive: function (obj) {
            if (core.is.boolean(obj)) return true;
            if (core.is.date(obj)) return true;
            if (core.is.number(obj)) return true;
            if (core.is.string(obj)) return true;
            return false;
        },
        guid: function (obj) {
            if (!core.is.string(obj)) return false;
            return (/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(obj));
        }
    }
}.call();