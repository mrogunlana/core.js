core.strings = function() {
    var _replace = function (str, search, replace) {
        return core.strings.format(str, {find: search, replace: replace});
    };
    var _format = function (String, data) {
        var Ret = (String == undefined ? '' : String);

        if (!core.is.array(data)) {
            data = [data];
        }
        for (var i = 0; i < data.length; i++) {
            var aFind = data[i].find.split('');
            for (var n = 0; n < aFind.length; n++)
                if (aFind[n].match(/[\[\\\^\$\.\|\?\*\+\(\)\{\}\]]/))
                    aFind[n] = '\\' + aFind[n];
            Ret = Ret.replace(new RegExp(aFind.join(''), 'g'), data[i].replace);
        }
        return Ret;
    };
    var _trim = function (str) {
        str = _trimLeft(str);
        return _trimRight(str);
    };
    var _trimLeft = function (str) {
        if (core.is.string(str))
            str = str.replace(/^\s+/, '');
        return str;
    };
    var _trimRight = function (str) {
        if (core.is.string(str))
            for (var i = str.length - 1; i >= 0; i--) {
                if (/\S/.test(str.charAt(i))) {
                    str = str.substring(0, i + 1);
                    break;
                }
            }
        return str;
    };
    var _padLeft = function (str, pad, len) {
        if (pad === undefined || pad === null) pad = " ";
        if (str === undefined || str === null) str = "";

        pad = pad + "";
        str = str + "";

        return (len <= str.length) ? str + "": _padLeft(pad + (str || "") + "", pad, len);
    };
    var _padRight = function (str, pad, len) {
        if (pad === undefined || pad === null) pad = " ";
        if (str === undefined || str === null) str = "";

        pad = pad + "";
        str = str + "";

        return (len <= str.length) ? str + "": _padRight((str || "") + (pad || " ") + "", pad, len);
    };
    return {
        replace: function (str, search, replace) {
            return _replace.apply(this, arguments);
        },
        format: function (str, replacements) {
            return _format.apply(this, arguments);
        },
        trim: {
            all: function (str) {
                return _trim.apply(this, arguments);
            },
            right: function (str) {
                return _trimRight.apply(this, arguments);
            },
            left: function (str) {
                return _trimLeft.apply(this, arguments);
            }
        },
        padding: {
            left: function (str, padding, length) {
                return _padLeft.apply(this, arguments);
            },
            right: function (str, padding, length) {
                return _padRight.apply(this, arguments);
            }
        }
    }
}.call();