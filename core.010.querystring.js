core.queryString = function() {
    var _qs = undefined;
    var _create = function (obj) {
        var rtn = '';
        for (var i in obj) {
            if (i)
                rtn += encodeURI(i.toString()) + '=' + encodeURI((obj[i] ? obj[i] : ''));
            else
                rtn += encodeURI((obj[i] ? obj[i] : ''));
            rtn += '&';
        }
        if (rtn)
            rtn = "?" + rtn.substring(0, rtn.length - 1);
        return rtn;
    };
    var _get = function (key) {
        if (!_qs) {
            _qs = new Object();
            var query = window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                if (pair.length == 2)
                    _qs[decodeURI(pair[0])] = decodeURI(pair[1]);
                else if (pair[0].length)
                    _qs[""] = decodeURI(pair[0]);
            }
        }
        if (key != undefined) {
            for (var x in _qs)
                if (x.toLowerCase() == key.toLowerCase()) key = x;
            return _qs[key];
        }
        return core.objects.clone(_qs);
    };

    return {
        "get": function() { return _get() },
        "create": function(obj) { return _create(obj); }
    }
}.call();
