core.className = function() {
    var _check = function (Class, className) {
        var sClass = "";
        var sClassName = " " + className + " ";
        if (core.is.HTMLElement(Class)) {
            sClass = Class.getAttribute("class");
        }
        else {
            sClass = Class;
        }

        sClass = " " + sClass + " ";

        if (sClass.indexOf(sClassName) == -1)
            return false;
        else
            return true;
    };
    var _remove = function (Class, className) {
        var sClass = Class;
        var sClassName = className;
        if (core.is.HTMLElement(Class)) 
            sClass = Class.getAttribute("class") || "";

        var aClassName = sClass.split(" ");
        aClassName = core.arrays.clean(aClassName);
        aClassName = core.arrays.remove(aClassName, sClassName);

        return aClassName.join(" ");
    };
    var _add = function (Class, className) {
        return _remove(Class, className) + " " + className;
    };
    var _replace = function (Class, oldClassName, newClassName) {
        var sClass = Class;
        if (core.is.HTMLElement(Class)) {
            sClass = Class.getAttribute("class");
        }

        var aClassName = sClass.split(" ");
        aClassName = core.arrays.clean(aClassName);

        aClassName = core.arrays.replace(aClassName, oldClassName, newClassName);

        return aClassName.join(" ");
    };
    return {
        check: function (Class, className) {
            return _check(Class, className);
        },
        remove: function (Class, className) {
            return _remove(Class, className);
        },
        add: function (Class, className) {
            return _add(Class, className);
        },
        replace: function (Class, oldClassName, newClassName) {
            return _replace(Class, oldClassName, newClassName);
        }
    }
}.call();