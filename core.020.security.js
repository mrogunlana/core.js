core.security = function() {
    var __user = {};
    function _user() {
        if (core.is.object(arguments[0])) {
            __user = arguments[0];
        }
        return __user;
    }
    return {
        user: function() {
            return _user.apply(this, arguments);
        }
    }
}.call();