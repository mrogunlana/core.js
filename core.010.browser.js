core.browser = function() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return {name: 'IE ', version: (tem[1] || '')};
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return {name: 'Opera', version: tem[1]};
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    if (M[0] == 'MSIE') {
        M[0] = 'IE';
    }

    //TODO: update browswer sniffer. See below: 
    //http://www.javascriptkit.com/javatutors/navigator.shtml
    //https://www.google.com/search?q=ie+11+rv+token&oq=ie+11+rv+token&aqs=chrome..69i57.4831j0j7&sourceid=chrome&es_sm=93&ie=UTF-8
    //window.setTimeout('core.browser.compatible.check()', 250);

    var _fontCheck = function (fontName) {
        var calculateWidth, monoWidth, serifWidth, sansWidth, width;
        var body = document.body;
        var container = document.createElement('div');
        var containerCss = [
            'position:absolute',
            'width:auto',
            'font-size:128px',
            'left:-99999px'
        ];

        // Create a span element to contain the test text.
        // Use innerHTML instead of createElement as it's smaller
        container.innerHTML = '<span style="' + containerCss.join(' !important;') + '">' +
        Array(100).join('wi') +
        '</span>';
        container = container.firstChild;

        calculateWidth = function (fontFamily) {
            container.style.fontFamily = fontFamily;

            body.appendChild(container);
            width = container.clientWidth;
            body.removeChild(container);

            return width;
        };

        // Pre calculate the widths of monospace, serif & sans-serif
        // to improve performance.
        monoWidth = calculateWidth('monospace');
        serifWidth = calculateWidth('serif');
        sansWidth = calculateWidth('sans-serif');

        return monoWidth !== calculateWidth(fontName + ',monospace') ||
            sansWidth !== calculateWidth(fontName + ',sans-serif') ||
            serifWidth !== calculateWidth(fontName + ',serif');
    };

    return {
        location: {
            push: function(state, url) {
                window.history.pushState(state, "", url + window.location.search);
            },
            clearData: function() {
                window.history.pushState(undefined, "", window.location.pathname);
            }
        },
        name: M[0],
        version: M[1],
        fullName: (M[0] == 'IE' ? 'Internet Explorer' : M[0]),
        compatible: {
            list: [["opera", "10.10"], ["chrome", "3.0"], ["safari", "4.0"], ["firefox", "3.5"], ["ie", "8"]], /* 2010 */
            check: function () {
                var compatible = false;
                for (var i in core.browser.compatible.list) {
                    if (core.browser.compatible.list[i][0].toLowerCase() == core.browser.name.toLowerCase()) {
                        if (core.cast.number(core.browser.compatible.list[i][1], 2) < core.cast.number(core.browser.version, 2)) {
                            compatible = true;
                            break;
                        }
                    }
                }
                if (!compatible) {
                    core.browser.compatible.onFailure();
                }
            },
            onFailure: function () {
                var el = '<div id="coreBrowserCompatibility" style="text-align: center;">' + core.browser.fullName + ' [' + core.browser.version + '] IS NOT SUPPORTED.</div>';
                document.body.innerHTML = el + document.body.innerHTML;
            }
        },
        fontCheck: function (fontName) {
            return _fontCheck.apply(this, arguments);
        }
    };
}.call();
