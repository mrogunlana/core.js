core.format = function() {
    var _masks = {
        string: {
            "phone": "(999) 999-9999",
            "phoneextn": "(999) 999-9999 X:99999",
            "ssn": "999-99-9999",
            "last4Ofssn": "***-**-9999",
            "last4OfCC": "****-****-****-9999",
            "zipcode": "99999-9999",
            "height": "999",
            "weight": "999",
            "year": "9999",
            "bankruptcycasenumber": "99-99999",
            "mtcn": "9999999999",
            "shortDate": "99/99/9999",
            "stateName": function (val) {
                var list = {"": ""};
                return (list[val] || '');
            }
        },
        number: {
            "currency": { thousandsSeparator: ",", decimalPlaces: 2, prefix: "$" },
            "currency0": { thousandsSeparator: ",", decimalPlaces: 0, prefix: "$" },
            "currency1": { thousandsSeparator: ",", decimalPlaces: 1, prefix: "$" },
            "currency2": { thousandsSeparator: ",", decimalPlaces: 2, prefix: "$" },
            "currency3": { thousandsSeparator: ",", decimalPlaces: 3, prefix: "$" },
            "currency4": { thousandsSeparator: ",", decimalPlaces: 4, prefix: "$" },
            "currency5": { thousandsSeparator: ",", decimalPlaces: 5, prefix: "$" },
            "currency6": { thousandsSeparator: ",", decimalPlaces: 6, prefix: "$" },
            "number": { thousandsSeparator: ",", decimalPlaces: 0, prefix: "" },
            "number1": { thousandsSeparator: ",", decimalPlaces: 1, prefix: "" },
            "number2": { thousandsSeparator: ",", decimalPlaces: 2, prefix: "" },
            "number3": { thousandsSeparator: ",", decimalPlaces: 3, prefix: "" },
            "number4": { thousandsSeparator: ",", decimalPlaces: 4, prefix: "" },
            "number5": { thousandsSeparator: ",", decimalPlaces: 5, prefix: "" },
            "number6": { thousandsSeparator: ",", decimalPlaces: 6, prefix: "" },
            "percent": { thousandsSeparator: ",", decimalPlaces: 0, prefix: "", suffix: " %" },
            "percent1": { thousandsSeparator: ",", decimalPlaces: 1, prefix: "", suffix: " %" },
            "percent2": { thousandsSeparator: ",", decimalPlaces: 2, prefix: "", suffix: " %" },
            "percent3": { thousandsSeparator: ",", decimalPlaces: 3, prefix: "", suffix: " %" },
            "percent4": { thousandsSeparator: ",", decimalPlaces: 4, prefix: "", suffix: " %" },
            "percent5": { thousandsSeparator: ",", decimalPlaces: 5, prefix: "", suffix: " %" },
            "percent6": { thousandsSeparator: ",", decimalPlaces: 6, prefix: "", suffix: " %" },
            "integer": { thousandsSeparator: ",", decimalPlaces: 0, prefix: "" }
        },
        date: {
            "default": "mm/dd/yyyy HH:MM:ss.lll Z",
            "shortDate": "mm/dd/yyyy",
            "mediumDate": "mmm d, yyyy",
            "longDate": "mmmm d, yyyy",
            "fullDate": "dddd, mmmm d, yyyy",
            "shortTime": "h:MM TT",
            "mediumTime": "h:MM:ss TT",
            "longTime": "h:MM:ss TT Z",
            "isoDate": "yyyy-mm-dd",
            "isoTime": "HH:MM:ss",
            "isoDateTime": "yyyy-mm-dd'T'HH:MM:ss",
            "isoUtcDateTime": "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",
            "daysHoursMin": "mm/dd/yyyy hh:MM TT"
        }
    };

    function _to(val, mask) {
        if (!mask) return val;
        if (core.is.date(val)) return _date(val, mask);
        if (_masks.number[mask]) return _number(val, mask);
        if (core.is.function(_masks.string[mask])) return _masks.string[mask](val);
        return _string(val, mask);
    };
    function _date(date, mask, utc) {
        mask = _masks.date[mask] || mask || _masks.date["default"];

        var i18n = {
            dayNames:
                    [
                    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
                    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
                    ],
            monthNames:
                    [
                    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                    ]
        };
        var token = /d{1,4}|m{1,4}|yy(?:yy)?|l{1,3}|([HhMsTt])\1?|[loSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        date = core.cast.date(date);

        if (!core.is.date(date)) return undefined;

        mask = (mask || _masks.date["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
        d = date[_ + "Date"](),
        D = date[_ + "Day"](),
        m = date[_ + "Month"](),
        y = date[_ + "FullYear"](),
        H = date[_ + "Hours"](),
        M = date[_ + "Minutes"](),
        s = date[_ + "Seconds"](),
        L = date[_ + "Milliseconds"](),
        o = utc ? 0 : date.getTimezoneOffset(),
        flags = {
            d: d,
            dd: pad(d),
            ddd: i18n.dayNames[D],
            dddd: i18n.dayNames[D + 7],
            m: m + 1,
            mm: pad(m + 1),
            mmm: i18n.monthNames[m],
            mmmm: i18n.monthNames[m + 12],
            yy: String(y).slice(2),
            yyyy: y,
            h: H % 12 || 12,
            hh: pad(H % 12 || 12),
            H: H,
            HH: pad(H),
            M: M,
            MM: pad(M),
            s: s,
            ss: pad(s),
            l: Math.round(L / 100),
            ll: pad(Math.round(L / 10)),
            lll: L,
            t: H < 12 ? "a" : "p",
            tt: H < 12 ? "am" : "pm",
            T: H < 12 ? "A" : "P",
            TT: H < 12 ? "AM" : "PM",
            Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
            o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
            S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
        };

        return mask.replace(token, function (str) {
            return str in flags ? flags[str] : str.slice(1, str.length - 1);
        });
    };
    function _number(val, mask) {
        mask = _masks.number[mask] || mask || "";
        if (!mask) return val;

        val = (val || 0);
        val = val.toString().replace(/\$|\,|%|\s/g, '');

        if (isNaN(val))
            val = "0";

        var o = Number(val).toFixed(mask.decimalPlaces ? mask.decimalPlaces : 0);
        var c = o.toString().substring(o.toString().indexOf('.') + 1)
        var s = (val == (val = Math.abs(val)));

        val = Math.floor(val).toString();

        for (var i = 0; i < Math.floor((val.length - (1 + i)) / 3) ; i++)
            val = val.substring(0, val.length - (4 * i + 3)) + mask.thousandsSeparator + val.substring(val.length - (4 * i + 3));

        return (((s) ? '' : '-') + mask.prefix + val + (mask.decimalPlaces > 0 ? '.' + c.toString() : '')) + (mask.suffix ? mask.suffix : '');
    };
    function _string(val, mask) {
        mask = _masks.string[mask] || mask || "";
        if (!mask) return val;
        if (!core.is.string(val)) return val;

        var rtn = "";

        if (val == undefined || val == null)
            val = "";

        var mPos = 0;
        var vPos = 0;
        var mChar = "";
        var vChar = "";

        while (true) {
            if (mPos == mask.length) {
                //rtn += val.substring(vPos);
                break;
            }
            else if (vPos == val.length) {
                break;
            }
            else {
                mChar = mask.substr(mPos, 1);
                vChar = val.substr(vPos, 1);
                switch (mChar) {
                    case "*":
                        rtn += "*";
                        vPos++;
                        break;
                    case "a":
                        vPos++;
                        while (/[a-zA-Z]/.test(vChar) == false) {
                            if (vPos == val.length) {
                                vChar = "";
                                break;
                            }
                            vChar = val.substr(vPos, 1);
                            vPos++;
                        }
                        rtn += vChar;

                        break;
                    case "9":
                        vPos++;
                        while (/[\d]/.test(vChar) == false) {
                            if (vPos == val.length) {
                                vChar = "";
                                break;
                            }
                            vChar = val.substr(vPos, 1);
                            vPos++;
                        }
                        rtn += vChar;

                        break;
                    case "z":
                        rtn += vChar;
                        vPos++;
                        break;
                    default:
                        rtn += mChar;
                        break;
                }
                mPos++;
            }
        }
        return rtn;
    };
    function _from(val, mask) {
        if (_masks.date[mask]) return core.cast.date(val);
        if (_masks.number[mask]) return core.cast.number(val);

        mask = _masks.string[mask] || mask || "";
        if (!mask) return val;

        var rtn = "";

        if (!mask) return val;
        if (val == undefined || val == null)
            val = "";

        var mPos = 0;
        var vPos = 0;
        var mChar = "";
        var vChar = "";

        while (true) {
            if (mPos == mask.length) {
                rtn += val.substring(vPos);
                break;
            }
            else if (vPos == val.length) {
                break;
            }
            else {
                mChar = mask.substr(mPos, 1);
                vChar = val.substr(vPos, 1);
                switch (mChar) {
                    case "*":
                        if (vChar == "*") {
                            vPos++;
                            break;
                        }
                    case "a":
                    case "9":
                    case "z":
                        rtn += vChar;
                        vPos++;
                        break;
                    default:
                        if (vChar == mChar)
                            vPos++;
                        break;
                }
                mPos++;
            }
        }
        return rtn;
    };
    return {
        masks: {
            "get": function(type, key) {
                if (!type && !key) {
                    return core.objects.clone(_masks);
                }
                if (type && _masks[type]) {
                    if (key) {
                        return core.objects.clone(_masks[type][key]);
                    }
                    else {
                        return core.objects.clone(_masks[type]);
                    }
                }
            },
            "put": function(type, key, value) {
                if (core.arrays.contains(["string","number","date"], type)) {
                    if (key) {
                        if (value === undefined) {
                            delete _masks[type][key];
                        }
                        else {
                            _masks[type][key] = value;
                        }
                    }
                }
            }
        },
        to: function(val, mask) {
            return _to.apply(this, arguments);
        },
        from: function(val, mask) {
            return _from.apply(this, arguments);
        }
    }
}.call();