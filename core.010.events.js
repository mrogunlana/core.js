core.events = function() {
    var _add = function (el, eventName, fn) {
        if (!fn) return;

        var HandleName = core.strings.replace("core_events_{0}_", "{0}", eventName);
        var Enum = 0;

        while (el[HandleName + Enum]) {
            Enum++;
        }

        HandleName += Enum;

        if (core.is.string(fn)) {
            el[HandleName] = new Function(fn);
        }
        else {
            el[HandleName] = fn;
        }

        if (el.addEventListener) {
            //W3C Standard
            el.addEventListener(eventName, function (e) {
                e = _get(e);
                return el[HandleName](e);
            }, false);
        }
        else {
            //IE
            el.attachEvent("on" + eventName, function (e) {
                e = _get(e);
                return el[HandleName](e);
            });
        }
        return HandleName;
    };
    var _get = function (e) {
        if (window.event) e = window.event;

        if (!e.target) e.target = _source(e);

        if ((!e.pageX && !e.pageY) && (e.clientX || e.clientY)) {
            var b = document.body || {}; //IE7 body is null on onerror when caught as page loads
            e.pageX = e.clientX + b.scrollLeft + document.documentElement.scrollLeft;
            e.pageY = e.clientY + b.scrollTop + document.documentElement.scrollTop;
        }

        return e;
    };
    var _cancel = function (e) {
        e = _get(e);

        if (e) {
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
            if (e.preventDefault) e.preventDefault();
        }
    };
    var _source = function (e) {
        var T;
        if (e.target) T = e.target;
        else if (e.srcElement) T = e.srcElement;
        else if (e.currentTarget) T = e.currentTarget;

        /* Safari Bug : Allows Text Nodes to report as Target */
        if (core.is.HTMLText(T)) T = T.parentNode;

        return T;
    };
    return {
        add: function (el, eventName, fn) {
            _add.apply(this, arguments);
        },
        cancel: function (e) {
            _cancel.apply(this, arguments);
        },
        source: function (e) {
            _source.apply(this, arguments);
        }
    }
}.call();